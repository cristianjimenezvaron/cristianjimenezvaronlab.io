---
title: Home
name: Cristian Felipe Jiménez Varón
date: 2023-02-20T15:26:23-06:00

categories: [one]
tags: [two, three, four]
interestslabel: "Research interests"
interests:
 [
  - "Network time series"
  - "Quantile spectral analysis in time series"
  - "Nonparametric functional data analysis"
  - "Functional time series forecasting"
  - "Functional data outlier detection"
  - "Data visualization"
  - "Nonparametric statistics"
  - "Clustering analysis"
  - "High dimensional nonparametric statistics"
]
applicationslabel: Applications
applications:
 [
  - "Environmental"
  - "Economics"
  - "Biostatistics"
  - "Finance"
]

postdoctorallabel: Postdoctoral Experience

postdoctoral:
[
  dddd:
    course: "Research Associate in Statistics. [York Nest Node](https://nest-programme.ac.uk/)"
    university: "University of York, UK"
    years: "09/2024 - 09/2027"
    website: "https://www.york.ac.uk/maths/people/cristian-varon/"

educationlabel: Education

education:
  aaaaa:
    course: "Ph.D. in Statistics"
    university: "King Abdullah University of Science and Technology (KAUST)"
    years: "08/2020 - 05/2024"
    defense_date: "22/05/2024"
    thesis: "Visualization, Characterization, and Forecasting of Multivariate and Functional Time Series"
  
  bbb:
    course: "MSc in Applied Mathematics"
    university: "Universidad Nacional de Colombia"
    years: "08/2016 - 12/2017"

  ccc:
    course: "BSc in Industrial Engineering"
    university: "Universidad Nacional de Colombia"
    years: "08/2012 - 12/2017"

  zzz:
    course: "BSc in Chemical Engineering"
    university: "Universidad Nacional de Colombia"
    years: "01/2010 - 12/2015"
---



I am researcher in *time series and functional data analysis*. My research primarily focuses on the development of statistical methods for modeling various types of data commonly encountered in environmental, economic, and finance applications. 
